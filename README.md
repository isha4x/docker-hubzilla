# docker-hubzilla

Docker Image for [Hubzilla](https://project.hubzilla.org)

## Fail2ban
enable debug logging at loglevel 'normal'

**/etc/fail2ban/jail.local**
```
[hubzilla]
enabled = true
findtime = 300
bantime = 900
maxretry = 3
filter = hubzilla
port = http,https
logpath = /var/lib/docker/containers/*/*-json.log
logencoding = utf-8
```

**/etc/fail2ban/filter.d/hubzilla.conf**
```
[Definition]
failregex= ^.*auth\.php.*failed login attempt.*from IP <HOST>.*$
ignoreregex =
```